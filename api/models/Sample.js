/**
* Sample.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  attributes: {
    latitude: {
      type: 'float',
      required: true,
      max: 90,
      min: -90
    },
    longitude: {
      type: 'float',
      required: true,
      max: 180,
      min: -180
    },
    overrideGPS: {
      type: 'boolean',
      required: true
    },
    habitat: {
      type: 'string',
      enum: [
        'hardwood forest (deciduous)',
        'softwood forest (coniferous)',
        'mixed forest',
        'agriculture',
        'grassland',
        'shrub',
        'wetland/riparian',
        'lawn',
        'garden/flower bed',
        'ditch'
      ],
      required: true
    },
    paths: {
      type: 'boolean',
      required: true
    },
    roads: {
      type: 'boolean',
      required: true
    },
    buildings: {
      type: 'boolean',
      required: true
    },
    fishing: {
      type: 'boolean',
      required: true
    },
    grazing: {
      type: 'boolean',
      required: true
    },
    crops: {
      type: 'boolean',
      required: true
    },
    litterLayer: {
      type: 'float',
      required: true,
      min: 0
    },
    litterLayerUnits: {
      type: 'string',
      enum: [
        'cm',
        'mm',
        'in'
      ],
      required: true,
      defaultsTo: 'cm'
    },
    lakeOrStream: {
      type: 'boolean',
      required: true
    },
    sampledAt: {
      type: 'datetime',
      required: true
    },
    weather: {
      type: 'string',
      required: true
    },
    groundMoisture: {
      type: 'string',
      enum: [
        'unknown',
        'dry',
        'medium',
        'wet'
      ],
      required: true
    },
    samplingMethod: {
      type: 'string',
      enum: [
        'mustard extraction',
        'flip and strip',
        'hand sample'
      ],
      required: true
    },
    middenCount: {
      type: 'integer',
      min: 0
    },
    standardPlot: {
      type: 'boolean',
      required: true
    },
    foundWorms: {
      type: 'boolean',
      required: true
    },
    scientist: {
      type: 'string',
      enum: [
        'elementary school student',
        'junior high school student',
        'high school student',
        'post-secondary student',
        'teacher',
        'biologist',
        'citizen'
      ],
      required: true
    },
    scientistAge: {
      type: 'integer',
      min: 0,
      max: 123
    },
    scientistOther: {
      type: 'string'
    },
    groupId: {
      type: 'string'
    },
    publicState: {
      type: 'string',
      enum: ['hidden', 'partial', 'public', 'historical'],
      required: true,
      defaultsTo: 'partial'
    },
    appVersion: {
      type: 'string',
      required: true
    },
    sighting: {
      model: 'sighting'
    }
  },
  beforeValidate: function (values, cb) {
    //the app isn't sending the right thing. Need a quick hack fix for no standardPlay/worms
    if (values.standardPlot == 'No') {
      values.standardPlot = 0;
    }
    if (values.foundWorms == 'No') {
      values.foundWorms = 0;
    }


    if (values.habitat) {
      values.habitat = values.habitat.toLowerCase();
    }

    if (values.groundMoisture) {
      values.groundMoisture = values.groundMoisture.toLowerCase();
    }

    if (values.samplingMethod) {
      values.samplingMethod = values.samplingMethod.toLowerCase();
    }

    if (values.scientist) {
      values.scientist = values.scientist.toLowerCase();
    }

    if (values.publicState) {
      values.publicState = values.publicState.toLowerCase();
    }

    cb();
  }
};

