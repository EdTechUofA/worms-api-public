/**
 * SampleController
 *
 * @description :: Server-side logic for managing samples
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var Promise = require('bluebird');
var fs = Promise.promisifyAll(require('fs'));
var util = require('util');
var json2csv = Promise.promisify(require('json2csv'));
var _ = require('lodash');

var _cleanUpFiles = function (uploadedFiles) {
  uploadedFiles.map(function (val) {
    return fs.unlinkAsync(val['fd']);
  });

  return Promise.all(uploadedFiles);
};


var _standardFind = function (req) {

  var where = _.defaults({publicState: ['public', 'partial', 'historical']}, actionUtil.parseCriteria(req));
  var query = Sample.find()
  .where(where)
  // .limit(actionUtil.parseLimit(req)) //THIS DEFAULTS REALLY LOW, SO DISABLE FOR NOW
  .skip(actionUtil.parseSkip(req))
  .sort(actionUtil.parseSort(req));

  return actionUtil.populateRequest(query, req);
};


//stolen from sails/lib/hooks/blueprints/actionUtil because that's how 'where' gets parsed
var _tryToParseJSON = function (json) {
  if (!_.isString(json)) return null;
  try {
    return JSON.parse(json);
  }
  catch (e) { return e; }
}

module.exports = {
  create: function (req, res, next) {
    var sighting = false;
    var sample = false;
    var sampleData = _.cloneDeep(req.body);
    var sightingData = sampleData.sighting;
    delete sampleData.sighting;

    //HACK BUG IN APP!! Fields sending 'No' instead of something boolean like
    if (sampleData.foundWorms === 'No') {
      sampleData.foundWorms = false;
    }
    if (sampleData.standardPlot === 'No') {
      sampleData.standardPlot = false;
    }

    //HACK!!
    //1/0 is sometimes sent by app.
    //thought it was safe, but turns out it isn't
    var brokenBooleans = {
      sample: [
        'overrideGPS',
        'paths',
        'roads',
        'buildings',
        'fishing',
        'grazing',
        'crops',
        'lakeOrStream'
      ],
      sighting: [
        'clitellumAbsent',
        'diamGT2mm',
        'colourGradient',
        'tailFlattens'
      ]
    };
    brokenBooleans.sample.map(function (field) {
      if (sampleData[field] === '1') {
        sampleData[field] = true;
      }
      else if (sampleData[field] === '0') {
        sampleData[field] = false;
      }
    });
    brokenBooleans.sighting.map(function (field) {
      if (sightingData[field] === '1') {
        sightingData[field] = true;
      }
      else if (sightingData[field] == '0') {
        sightingData[field] = false;
      }
    });
    //END HACK

    req.file('sighting[image]').upload(
      {
        maxBytes: 10*1024*1024, //10 MB.
        dirname: '../../uploads' //Sails default uses the ./.tmp/uploads so need to come up twice from that to get ./
      },
      function (err, uploadedFiles) {
        if (err) {
          if (err.code = 'E_EXCEEDS_UPLOAD_LIMIT') {
            err.status = 400; //400 error instead of 500 error.
          }
          return res.negotiate(err);
        }

        if (uploadedFiles.length > 1) {
          //remove clean up files because strays are bad
          _cleanUpFiles(uploadedFiles).then(function (results) {
            return res.badRequest({message: 'Too many files.'});
          });
        }
        else if (uploadedFiles.length === 1) {
          if (!sightingData) {
            sightingData = {};
          }

          sightingData.image = util.format('/uploads/%s', uploadedFiles[0]['fd'].split('/').pop());
        }

        Sample.create(sampleData).then(function (newSample) {
          sample = newSample;
          if (sightingData) {
            sightingData.sample = sample.id;
            return Sighting.create(sightingData);
          }

          return false;
        }).then(function (newSighting) {
          if (newSighting) {
            sighting = newSighting;
            return Sample.update(sample.id,{sighting: sighting.id});
          }

          return [sample];
        }).then(function (samples) {
          sample = samples.pop();
          if (sighting) {
            sample.sighting = sighting; //assign sighting by hand to save a .find().populate() since update().populate() doesn't work
          }

          // Send JSONP-friendly response if it's supported
          // (HTTP 201: Created)
          res.status(201);
          return res.ok(sample.toJSON());
        }).catch(function (err) {
          var jobs = [
            _cleanUpFiles(uploadedFiles) //remove stray image files, cause strays are bad mmkay
          ];

          if (sample) {
            jobs.push(Sample.destroy({id: sample.id})); //remove stray samples
          }

          if (sighting) {
            jobs.push(Sighting.destroy({id: sighting.id})); //remove stray sightings
          }

          return Promise.all(jobs).then(function () {
            return res.negotiate(err);
          })
        });
      }
    )
  },
  output_kml: function (req, res, next) {
    var jobs = [
      _standardFind(req)
    ];
    var highlight = req.query.highlight;

    var locals = {
      layout: null
    };

    if (req.query.highlight) {
      if (_.isString(highlight)) {
        highlight = _tryToParseJSON(highlight);
      }

      jobs.push(Sample.find({
        // where: req.query.highlight
        select: ['id'],
        where: highlight
      }));
    }

    return Promise.all(jobs).spread(function (samples, highlightedSamples) {
      highlightedSamples = highlightedSamples || [];
      highlightedSamples = highlightedSamples.map(function (sample) {
        return sample.id;
      });
      locals.samples = samples.map(function (sample) {
        var prefix = (sample.sighting && sample.sighting.wormCount > 0) ? '' : 'No';
        if (_.includes(highlightedSamples, sample.id)) {
          sample._wormsIcon = prefix + 'WormsGrp';
        }
        else if (sample.publicState == 'historical') {
          sample._wormsIcon = prefix + 'WormsSci';
        }
        else {
          sample._wormsIcon = prefix + 'WormsPub';
        }

        return sample;
      });

      res.setHeader('Content-Type', 'application/vnd.google-earth.kml+xml');

      return res.view('sample/kml', locals);
    });
  },
  output_csv: function (req, res, next) {
    _standardFind(req).exec(function (err, samples) {
      if (err) return res.negotiate(err);

      samples = samples.map(function (sample) {
        if (sample.sighting && sample.publicState != 'public') {
          delete sample.sighting.notes;
        }

        return sample;
      });

      json2csv({
        data: samples,
        fields: [
          'latitude',
          'longitude',
          'overrideGPS',
          'sighting.wormCount',
          'sampledAt',
          'samplingMethod',
          'sighting.classification',
          'sighting.length',
          'groundMoisture',
          'habitat',
          'litterLayer',
          'litterLayerUnits',
          'lakeOrStream',
          'paths',
          'roads',
          'buildings',
          'fishing',
          'grazing',
          'crops',
          'sighting.notes'
        ]
      }).then(function (csv) {
        res.setHeader('Content-disposition', 'attachment; filename=sample.csv');
        res.setHeader('Content-Type', 'text/csv');

        return res.end(csv);
      }).catch(res.negotiate);
    });
  }
};

